package com.stackroute.strings;

import java.util.Scanner;

public class LongestSubString {
    public static final String RESPONSE_NOT_FOUND = "Longest substring is not present in the given StringBuilder";
    public static final String RESPONSE_INVALID_INPUT = "Give proper input";

    public static void main(String[] args) {
        LongestSubString longestSubString = new LongestSubString();
        longestSubString.startProgram();     
    }

    private void startProgram() {
        StringBuilder userInput = getInput();
        StringBuilder result = findLongestSubString(userInput);
        // displayResult(result);
    }

    private StringBuilder getInput() {
        Scanner scanner = new Scanner(System.in);
        StringBuilder userInput = new StringBuilder(scanner.nextLine());

        scanner.close();

        return userInput;
    }

    //write logic to find the longest substring that appears at both ends of a given string and return result
    /**
     * -Should accept StringBuilder as input and return StringBuilder
     * -Should find the longest substring that appears at both ends of a given StringBuilder
     * -Should return "Longest substring is not present in the given StringBuilder" if longest substring does not exist
     * -Should return "Give proper input" if input is empty StringBuilder
     * @param input
     * @return
     */
    public StringBuilder findLongestSubString(StringBuilder input) {
        StringBuilder result = new StringBuilder();
        int searchRestartPosition = 0; // Has to starts at 1 else all substrings are itself
        boolean solutionFound = false;

        
        // Null check
        if (input == null || input.toString().equals("") ) {
            result = result.append(RESPONSE_INVALID_INPUT);

            return result;
        }

        // Babysit weird test case condition
        if (input.length() == 1) {
            result = input;

            return result;
        }

        while(!solutionFound) {
            //1. Locate anchor from 0+1 position
            for (int i = searchRestartPosition; i < input.length(); i++) {
                if (input.charAt(0) == input.charAt(i)) {
                    // 2. When a possible anchor is found, update starting point for next search
                    searchRestartPosition = i + 1;
                
                    // 3. Extract the substring from anchor position to the end                
                    StringBuilder substringFromAnchor = result.append(input.substring(i));
                    int anchorSubStringLength = substringFromAnchor.length();

                    StringBuilder substringFromStart = new StringBuilder(input.substring(0, anchorSubStringLength));

                    System.out.println("substringFromAnchor: " + substringFromAnchor);
                    System.out.println("substringFromStart: " + substringFromStart);

                    // 4. If the substrings NOT match, search next occurance
                    if(substringFromAnchor.equals(substringFromStart)) {
                        solutionFound = true;

                        break;
                    } else {
                        result = result.delete(0, result.length());
                    }
                }
            }

            // 5. All position checked and substring NOT found            
            solutionFound = true;
            result = new StringBuilder(RESPONSE_NOT_FOUND);
        }

        return result;
    }
}
